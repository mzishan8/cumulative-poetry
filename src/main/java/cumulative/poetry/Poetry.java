package cumulative.poetry;

public class Poetry {
    public static void main(String args[]){
        Poet poet= new Poet();
        System.out.println(args[0]);
        if(args.length > 0) {
            if(args[0].equals("--reveal-for-day")){
                try {
                    System.out.println(poet.recite(Integer.parseInt(args[args.length-1])));
                }catch (Exception ex){
                    System.out.println(ex);
                }
            }else if(args[0].equals("--recite")){
                for(int i=1 ;i < 12; i++){
                    System.out.println("Day "+i);
                    System.out.println(poet.recite(i));
                }
            }
        }else{
            System.out.println("Wrong input given");
        }
    }
}
